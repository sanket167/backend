var jwt = require('jsonwebtoken');
var expressJwt = require('express-jwt');
var config = require('../config/config');
var checkToken = expressJwt({ secret: config.secrets.jwt });
var User = require('../api/user/userModel');
var signToken = require('./auth').signToken;
var gt;
var tok;
// var token = signToken(req.user._id);
// console.log(token);
exports.decodeToken = function() {
  return function(req, res, next) {
    var token = signToken2();
    //console.log(token);
    // make it optional to place token on query string
    // if it is, place it on the headers where it should be
    // so checkToken can see it. See follow the 'Bearer 034930493' format
    // so checkToken can see it and decode it
    if (req.query && req.query.hasOwnProperty('access_token')) {
      req.headers.authorization = 'Bearer ' + req.query.access_token;
    }
    else 
    {
      req.headers.authorization = 'Bearer ' + token;
    }

    // this will call next if token is valid
    // and send error if its not. It will attached
    // the decoded token to req.user
    checkToken(req, res, next);
  };
};

exports.getFreshUser = function() {
  return function(req, res, next) {
    if(req.post._id) // if you are deleting post then first check user id
    {
     var req_id=req.user._id; //logged in user
     var post_author_id=req.post.author._id; // person whose post we are deleting
     console.log(req_id);
     console.log(post_author_id);
     if(req_id!=post_author_id)
     {
      res.status(401).send('Unauthorized');
      return;
     }
    }
  //  //console.log(req.post.author._id);
    User.findById(req.user._id)
      .then(function(user) {
        if (!user) {
          // if no user is found it was not
          // it was a valid JWT but didn't decode
          // to a real user in our DB. Either the user was deleted
          // since the client got the JWT, or
          // it was a JWT from some other source
          res.status(401).send('Unauthorized');
        } else {
          // update req.user with fresh user from
          // stale token data
          req.user = user;
          next();
        }
      }, function(err) {
        next(err);
      });
  }
};

exports.verifyUser = function() {
  return function(req, res, next) {
    var username = req.body.username;
    var password = req.body.password;

    // if no username or password then send
    if (!username || !password) {
      res.status(400).send('You need a username and password');
      return;
    }

    // look user up in the DB so we can check
    // if the passwords match for the username
    User.findOne({username: username})
      .then(function(user) {
        if (!user) {
          res.status(401).send('No user with the given username');
        } else {
          // checking the passowords here
          if (!user.authenticate(password)) {
            res.status(401).send('Wrong password');
          } else {
            // if everything is good,
            // then attach to req.user
            // and call next so the controller
            // can sign a token from the req.user._id
            req.user = user;
            next();
          }
        }
      }, function(err) {
        next(err);
      });
  };
};



signToken2 = function() {
  gt = tok;
  //console.log(gt);
  return gt;
};

// util method to sign tokens on signup
exports.signToken = function(id) {
  tok = jwt.sign(
    {_id: id},
    config.secrets.jwt,

  );

  return tok;
};
